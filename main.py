from audioop import reverse
from tkinter import W
import fitz
from fpdf import FPDF

print("Add (.pdf) file name in your local current directory:")
filename = input()

try:
    fitz.open(filename)
    doc = fitz.open(filename)
except:
    print("No file found with specified name. (Don't forget to add .pdf as extension in filename)\n")    
    exit()

pdf = FPDF()
pdf.add_page()
pdf.set_font("Arial", size = 10)

no_of_pages = len(doc)

print("From page number:")
from_page = int(input())

print("To page number:")
to_page = int(input())

pdf_overall_text = []
for p in range(from_page-1,to_page):
    page = doc[p]
    text = page.get_text("text")
    # list to store the co-ordinates of all highlights
    coordinates = []
    # loop till we have highlight annotation in the page
    annot = page.first_annot

    while annot:
        if annot.type[0] == 8:
            all_coordinates = annot.vertices
            if len(all_coordinates) == 4:
                highlight_coord = fitz.Quad(all_coordinates).rect
                coordinates.append(highlight_coord)
            else:
                all_coordinates = [all_coordinates[x:x+4] for x in range(0, len(all_coordinates), 4)]
                for i in range(0,len(all_coordinates)):
                    coord = fitz.Quad(all_coordinates[i]).rect
                    coordinates.append(coord)
                    # print(coord)
        annot = annot.next
    all_words = page.get_text_words()
    # List to store all the highlighted texts
    highlight_text = []
    highlights = sorted(coordinates, key=lambda x: -x[1])

    for h in highlights:
        sentence = [w[4] for w in all_words if fitz.Rect(w[0:4]).intersects(h)]
        highlight_text.append(" ".join(sentence))
    highlight_text=list(reversed(highlight_text))
    pdf_overall_text += highlight_text
    
for x in pdf_overall_text:
    pdf.cell(500, 7, txt = '* ' + x, ln = 1, align = 'L')
  
    # text = "\n".join(highlight_text)
    # pdf.cell(200, 10, txt = text,
    #       align = 'C')
# print(pdf_overall_text)
pdf.output("extracted.pdf")  
    # print("\n".join(highlight_text))
print("Result: .pdf file generated with name-extracted.pdf. Do check in your current directory.")


    